/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.scrapingdirectories.insiderpages;

import org.scrapingdirectories.base.MetaData;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.scrapingdirectories.base.BasicReader;

/**
 *
 * @author DELL
 */
public class InsiderCategoriesUtils extends BasicReader{
  private String CATEGORIES_URL = "http://www.insiderpages.com/browse";
  
  public  String readCategories(String cat_url, String file)
  {
      String content = new String();
      try {
          content = readSimple(cat_url, new File(file));
          //System.out.println(content);
      } catch (IOException ex) {
          Logger.getLogger(InsiderCategoriesUtils.class.getName()).log(Level.SEVERE, null, ex);
      }
      return content;
  }
  
  public void parseMainCategories() throws IOException {
      //parse the main categories File
     Document doc = Jsoup.parse(FileUtils.readFileToString(new File(MetaData.STORE_MAIN_CATEGORIES_FILE)));
      
     List<String> categories = new ArrayList<String>();
     Elements a = doc.getElementById("search_biz_cats").select("strong a");     
     
      for (int i = 0; i < a.size(); i++) {
          String category = "http://www.insiderpages.com" + a.get(i).attr("href");

          //get the last / to parse the Category Name.
          int index = category.lastIndexOf("/");
          category = category.substring(index + 1, category.length());
          System.out.println(category);
          //add category
          categories.add(category);

          //read and download the subcategories into a folder.
          String temp = readCategories(category, MetaData.STORE_MAIN_CATEGORIES_DIR + i + ".html");

          //Flush to category file
          FileUtils.writeLines(new File(MetaData.CATEGORIES_FILE), categories);
          System.out.println("\nParsing Categories Ended..");
      } 
      
  }
  
  public void parseInsiderSubcategories() throws IOException{
      //get all files to be parse under Main_Categories Dir
     File f [] = new File(MetaData.STORE_MAIN_CATEGORIES_DIR).listFiles();
     
     List<String> subcategories = new ArrayList<String>();
     Document doc = null;
      for (int i = 0; i < f.length; i++) {
          System.out.println("Parsing file: "+ f[i].getName()+"\n");
          doc = Jsoup.parse(FileUtils.readFileToString(f[i]));
          Elements a = doc.select("div[class=content_column] a");
          
          for (int j = 0; j < a.size(); j++) {
              //get the subcategory link
              String link = a.get(j).attr("href");
              //quit the last /, we need the sub category name only
              int index = link.lastIndexOf("/");              
              link = link.substring(index + 1, link.length());
              
              System.out.println(link);
              //add links to the subcategories
              subcategories.add(link);              
          } // end for          
      } //end for
      
      //Flush to the subcategory file
      FileUtils.writeLines(new File(MetaData.SUB_CATEGORIES_FILE), subcategories);
      System.out.println("\nParsing SubCategories Ended..");
  }
  
}
