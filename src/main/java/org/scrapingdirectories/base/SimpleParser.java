/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.scrapingdirectories.base;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.scrapingdirectories.base.utils.DBUtils;

/**
 *
 * @author DELL
 */
public class SimpleParser {
    
    //Parse Google Pages and return the next page, or NOT_PAGE in case no more pages
    public SESimpleData doParseGoogle(File f) throws Exception
    {  
       SESimpleData sed = new SESimpleData(); 
       List<String> items = new ArrayList<String>();       
       Document doc = Jsoup.parse(FileUtils.readFileToString(f));
       
       //check if some captcha or similar print
       if (doc.html().toLowerCase().contains("captcha")) {
           Reports.totalCaptchaResponse++;
           //do something with the parsing
           if (!f.getName().contains("parsed-captcha-")) {
                f.renameTo(new File(MetaData.GOOGLE_DIR + "parsed-captcha" + f.getName() ));
           }
           throw new Exception("Captcha Found");
       }
       
       Elements a= doc.getElementById("ires").select("ol li h3 a");
        for (int i = 0; i < a.size(); i++) {
            String link = a.get(i).attr("href");            
            link = getGoogleLinkCleaned(link);            
            System.out.println("Google Link Parsed: " + link + " File: "+f.getName());
            Reports.totalSitesParsed++;
            try {
                if (GeneralUtils.applyContainsFilters(link)) {
                    items.add(link);
                    DBUtils.insertLink(link);
                    Reports.totalLinksRecordsStored++;
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        FileUtils.writeLines(new File(MetaData.TXT_TEMP_OUTPUT), items, true);
        
        if (!f.getName().startsWith("parsed")) {
            f.renameTo(new File(MetaData.GOOGLE_DIR + "parsed-" + f.getName() ));
        }
        
        try {
            sed.next_page = new URI("https://www.google.com" + doc.getElementById("nav").select("td[class=b][style=text-align:left] a").first().attr("href"));
        } catch (Exception ex) {
            sed.next_page = null;            
            System.out.println("No Pagination in Google file: "+f.getName());
        }
        
        return sed;
    }
    
    public String getGoogleLinkCleaned (String link) 
    {
        //get the second href
       int index = link.lastIndexOf("http");
       if (index > 0) {
           link = link.substring(index, link.length());
       }
       
       //get first % left
       index = link.indexOf("%");
       if (index > 0) {
          link = link.substring(0,index);
       }
       
       //get first &
       index = link.indexOf("&");
       if (index > 0) {
          link = link.substring(0,index);
       }
       return link;
    }
//--------------------------------------------------------------------------------
    public SESimpleData  doParseYahoo(File f) throws Exception
    {   
        SESimpleData sed = new SESimpleData();
        List<String> items = new ArrayList<String>();  
       Document doc = Jsoup.parse(FileUtils.readFileToString(f));
       //check if some captcha or similar print
       if (doc.html().toLowerCase().contains("captcha")) {
           Reports.totalCaptchaResponse++;
           
           if (!f.getName().contains("parsed-captcha-")) {
                f.renameTo(new File(MetaData.GOOGLE_DIR + "parsed-captcha" + f.getName() ));
           }
           
           throw new Exception("Captcha Found");
       }
       
       Elements a= doc.getElementById("web").select("ol li h3 a");
        for (int i = 0; i < a.size(); i++) {
            String link = a.get(i).attr("href");            
            System.out.println("Yahoo Link Parsed: " + link);
            Reports.totalSitesParsed++;
            try {
                if (GeneralUtils.applyContainsFilters(link)) {
                    items.add(link);
                    DBUtils.insertLink(link);
                    Reports.totalLinksRecordsStored++;
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        sed.items = items;
        FileUtils.writeLines(new File(MetaData.TXT_TEMP_OUTPUT), items, true);
        
        if (!f.getName().startsWith("parsed")) {
            f.renameTo(new File(MetaData.YAHOO_DIR + "parsed-" + f.getName() ));
        }
        
        try {
            sed.next_page =  new URI(doc.select("div[class=compPagination] a[class=next]").first().attr("href"));
        } catch (Exception ex) {
            sed.next_page = null;
            System.out.println("No Pagination in Yahoo file: "+f.getName());
        }
        
        return sed;
    }
//---------------------------------------------------------------------------------    
    public SESimpleData doParseBing(File f) throws Exception {
        SESimpleData sed = new SESimpleData();
        List<String> items = new ArrayList<String>();
        
          Document doc = Jsoup.parse(FileUtils.readFileToString(f));
          
          //check if some captcha or similar print
       if (doc.html().toLowerCase().contains("captcha")) {
           Reports.totalCaptchaResponse++;
           
           if (!f.getName().contains("parsed-captcha-")) {
                f.renameTo(new File(MetaData.GOOGLE_DIR + "parsed-captcha" + f.getName() ));
           }
           throw new Exception("Captcha Found");
       }
          
          Elements a= doc.getElementById("b_results").select("li h2 a");
          
        for (int i = 0; i < a.size(); i++) {
            String link = a.get(i).attr("href");            
            System.out.println("Bing Link Parsed: " + link);
            Reports.totalSitesParsed++;
            try {
                if (GeneralUtils.applyContainsFilters(link)) {
                    items.add(link);
                   DBUtils.insertLink(link);
                   Reports.totalLinksRecordsStored++;
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        sed.items = items;
        FileUtils.writeLines(new File(MetaData.TXT_TEMP_OUTPUT), items, true);
        if (!f.getName().startsWith("parsed")) {
            f.renameTo(new File(MetaData.BING_DIR + "parsed-" + f.getName() ));
        }
        
        try {
            sed.next_page = new URI("http://www.bing.com" + doc.select("a[class=sb_pagN]").first().attr("href"));
        } catch (Exception ex) {
            sed.next_page = null;
            System.out.println("No Pagination in Bing file: "+f.getName());
        }       
        
        return sed;
    }
//-------------------------------------------------------------------------------------
   
}
