/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.scrapingdirectories.base;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
public class MetaData {   
    public static String SITE ="NONE";
    
    public static String DMA_REGIONS_FILE ="data/metadata/dma_regions.txt";
    public static int DMA_FROM = 0;
    public static int DMA_TO = 0; 
    
    public static int MAX_POOL_THREADS = 100;
       
    //TXT FILE
    public static String TXT_TEMP_OUTPUT = "data/raw_output.txt";
    public static boolean DELETE_TXT_OUTPUT_FILE_AT_START = false;
    
    public static String FILTERS_CONFIG_FILE = "data/Filtersconfig.txt";
    public static List<String> linkFilters = new ArrayList<String>();
    
    //instance config file
    public static String INSTANCE_CONFIG_FILE = "data/instance.txt";
    
    //categories options
    public static String STORE_MAIN_CATEGORIES_FILE = "data/insiderpages/main_categories.html";
    public static String STORE_MAIN_CATEGORIES_DIR = "data/insiderpages/categories/";    
    public static String SUB_CATEGORIES_FILE ="data/metadata/sub_categories.txt";
    public static String CATEGORIES_FILE ="data/metadata/categories.txt";
    public static int CAT_FROM = 0;
    public static int CAT_TO = 0;
    public static boolean USE_CATEGORY = true;
    
    //db file
    public static boolean DB_DELETE_BASIC_LINK_TABLE = true;
    
    //temporal Files
    public static String GOOGLE_DIR ="data/temp/google/";
    public static String YAHOO_DIR ="data/temp/yahoo/";
    public static String BING_DIR ="data/temp/bing/";
    public static String OTHER_DIR ="data/temp/other/";
    public static boolean DELETE_TEMP_FILES = false;
    
    //Proxies
    public static String PROXY_FILE = "data/proxy.txt";
    public static boolean USE_PROXY = false;
    public static boolean CHECK_PROXY = false;
    
    public static int MAX_POOL_THREAD = 0;
    public static boolean DO_PARSE_ONLY = false;
}
