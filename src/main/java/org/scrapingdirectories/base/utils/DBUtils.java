/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.scrapingdirectories.base.utils;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class DBUtils {
    
    public static Connection connection = null;
    
    public static void connectToMySQL (String host, String port, String database, String username, String password) throws Exception
    {
       Class.forName("com.mysql.jdbc.Driver");
       connection = DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+database, username, password);     
    }
    
    public static boolean isConnected() throws SQLException {
     return !connection.isClosed();
    }
    
    public static void closeConnection() throws SQLException
    {
      connection.close();
    }
    
    public static ResultSet querySimple(String sql) throws SQLException {
        PreparedStatement query = connection.prepareStatement(sql);
        ResultSet rs = query.executeQuery();
        return rs;
    }
    
    public static void createH2Connection()throws Exception{
      Class.forName("org.h2.Driver");
        connection = DriverManager.getConnection("jdbc:h2:"+new File("data/db").getAbsolutePath());
        connection.setAutoCommit(true);      
    }
    
    public static void createBasicLinkTable() throws SQLException{        
             connection.createStatement().execute("CREATE TABLE basic_link (link VARCHAR(300), PRIMARY KEY(link));");            
    }
    
     public static int insertLink(String link) throws SQLException{        
            PreparedStatement ps=connection.prepareStatement("INSERT INTO basic_link values(?)");
            ps.setString(1, link);     
            return ps.executeUpdate();
     }
     
     public static List<String> getAllLinks() throws SQLException{
        List<String> links = new ArrayList<>();
        PreparedStatement ps=connection.prepareStatement("SELECT * FROM basic_link");
            ResultSet rs=ps.executeQuery();            
            while(rs.next()){
                links.add(rs.getString(1));
            }
        return links;
     }
     
     public static void deleteDataFromTable(String table) throws SQLException {      
            PreparedStatement ps = connection.prepareStatement("DELETE from "+table);
            ps.executeUpdate();        
    }
}
