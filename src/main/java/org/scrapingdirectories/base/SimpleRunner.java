/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.scrapingdirectories.base;

import java.io.File;
import java.io.IOException;
import java.net.Authenticator;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.commons.io.FileUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

/**
 *
 * @author DELL
 */
public class SimpleRunner {
     private PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();   
     protected List<BasicProxy> proxies;
  
//------------------------------------------------------------------------------------------------------------------------     
     public void Run(List<URI> processUrl) throws IOException, InterruptedException 
     {  
         //init the pool and HttpClient
         cm.setMaxTotal(MetaData.MAX_POOL_THREADS);
        CloseableHttpClient httpclient = HttpClients.custom()
                .setConnectionManager(cm)
                .build();   
        
        try {
        // create a thread for each URI            
            int yahoo_count = new File(MetaData.YAHOO_DIR).listFiles().length + 1,
                google_count = new File(MetaData.GOOGLE_DIR).listFiles().length + 1,
                bing_count = new File(MetaData.BING_DIR).listFiles().length + 1,
                other_count = new File(MetaData.OTHER_DIR).listFiles().length +1;            
            
        GetThread[] threads = new GetThread[processUrl.size()];
            for (int i = 0; i < threads.length; i++) {
                HttpGet httpget = new HttpGet(processUrl.get(i));
                
                if (httpget.getURI().getHost().contains("bing")) {
                    threads[i] = new GetThread(httpclient, httpget, bing_count, proxies);
                    bing_count++;
                } else if (httpget.getURI().getHost().contains("yahoo")) {
                    threads[i] = new GetThread(httpclient, httpget, yahoo_count, proxies);
                    yahoo_count++;
                } else if (httpget.getURI().getHost().contains("google")) {
                    threads[i] = new GetThread(httpclient, httpget, google_count, proxies);
                    google_count++;
                } else {
                    threads[i] = new GetThread(httpclient, httpget, other_count, proxies);
                    other_count++;
                }

            }
            
            
            //start the threads
        for (int j = 0; j < threads.length; j++) {
                threads[j].start();
            }
        
        // join the threads
            for (int j = 0; j < threads.length; j++) {
                threads[j].join();
            }
            
        } finally {
            httpclient.close();
        }    
    }
     

}
