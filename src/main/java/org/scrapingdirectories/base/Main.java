package org.scrapingdirectories.base;

import java.io.File;
import java.net.URI;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.scrapingdirectories.insiderpages.InsiderCategoriesUtils;

public class Main {
    /**
     * @param args the command line arguments
     */
    public static void main2(String[] args) throws Exception {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
           // HttpHost target = new HttpHost("www.google.com", 443, "https");
           /*HttpHost proxy = new HttpHost("127.0.0.1", 8080, "http");

            RequestConfig config = RequestConfig.custom()
                    .setProxy(proxy)
                    .build(); */
            //HttpGet request = new HttpGet("https://www.google.com/search");
            //request.addHeader("User-Agent", "Mozilla");
            
            URI uri = GeneralUtils.getBingQuery("flowers", "San Diego, CA", "insiderpages.com");/*new URIBuilder()
        .setScheme("https")
        .setHost("search.yahoo.com")
        .setPath("/search")
        .setParameter("p", "flowers in Manchester site:insiderpages.com")        
        .build();*/
HttpGet request = new HttpGet(uri);
System.out.println(request.getURI());
             
            System.out.println("Executing request " + request.getRequestLine() + " to " );

            CloseableHttpResponse response = httpclient.execute(request);
            try {
                System.out.println("----------------------------------------");
                System.out.println(response.getStatusLine());
      
                String raw_response = EntityUtils.toString(response.getEntity());
                Document doc = Jsoup.parse(raw_response);
                System.out.println(doc.text());
                System.out.println();
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        } 
               
        
    }

public static void main(String[] args) throws Exception {
        // Create an HttpClient with the ThreadSafeClientConnManager.
        // This connection manager must be used if more than one thread will
        // be using the HttpClient.
    /*MySQLUtils.connectToMySQL("localhost", "3306", "directories", "root", "");
    System.out.println(MySQLUtils.isConnected());
    String sql = "SELECT * FROM dma_regions";
    List<DMARegionsData> dmrd = GeneralUtils.parseDMARegionsFromResultSet(MySQLUtils.querySimple(sql));
    for (DMARegionsData d:dmrd) {
        System.out.println(d.getDmaRegions());
    }
    MySQLUtils.closeMySQL();
    */
    //System.out.println(GeneralUtils.getCategories().size());
    String category = GeneralUtils.getCategories().get(0);
    String dma_region = GeneralUtils.getDmaRegions().get(0);
    String site ="insiderpages.com";
    GeneralUtils.createDataTempDir(false);
    
    InsiderCategoriesUtils cu = new InsiderCategoriesUtils();
    SimpleRunner sr = new SimpleRunner();
   // sr.setUpUrlsFromDMAandCategories(site, 0, 1, 0, -1);
    //sr.Run();
   
    
    Control.initParameters();
    Control.doStart();
    //Print report
        GeneralUtils.printReport();
   // cu.readCategories();
    //cu.parseMainCategories();
    //cu.parseInsiderSubcategories();
    //System.out.println(GeneralUtils.getSubcategories().size());
    
      /*  PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(100);

        CloseableHttpClient httpclient = HttpClients.custom()
                .setConnectionManager(cm)                
                .build();
        try {
            // create an array of URIs to perform GETs on
            String[] urisToGet = {
                "http://hc.apache.org/",
                "http://hc.apache.org/httpcomponents-core-ga/",
                "http://hc.apache.org/httpcomponents-client-ga/",
            };

            // create a thread for each URI
            GetThread[] threads = new GetThread[urisToGet.length];
            for (int i = 0; i < threads.length; i++) {
                HttpGet httpget = new HttpGet(urisToGet[i]);
                threads[i] = new GetThread(httpclient, httpget, i + 1);
            }

            // start the threads
            for (int j = 0; j < threads.length; j++) {
                threads[j].start();
            }

            // join the threads
            for (int j = 0; j < threads.length; j++) {
                threads[j].join();
            }

        } finally {
            httpclient.close();
        }*/
    }

    /**
     * A thread that performs a GET.
     */
    static class GetThread extends Thread {

        private final CloseableHttpClient httpClient;
        private final HttpContext context;
        private final HttpGet httpget;
        private final int id;

        public GetThread(CloseableHttpClient httpClient, HttpGet httpget, int id) {
            this.httpClient = httpClient;
            this.context = new BasicHttpContext();
            this.httpget = httpget;
            this.id = id;
        }

        /**
         * Executes the GetMethod and prints some status information.
         */
        @Override
        public void run() {
            try {
                System.out.println(id + " - about to get something from " + httpget.getURI());
                CloseableHttpResponse response = httpClient.execute(httpget, context);
                try {
                    System.out.println(id + " - get executed");
                    // get the response body as an array of bytes
                    HttpEntity entity = response.getEntity();
                    if (entity != null) {
                        byte[] bytes = EntityUtils.toByteArray(entity);
                        System.out.println(id + " - " + bytes.length + " bytes read");
                    }
                } finally {
                    response.close();
                }
            } catch (Exception e) {
                System.out.println(id + " - error: " + e);
            }
        }

    }
    
    
}
