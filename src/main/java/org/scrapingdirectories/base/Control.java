/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.scrapingdirectories.base;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.scrapingdirectories.base.utils.DBUtils;

/**
 *
 * @author DELL
 */
public class Control {
    
    private static List<URI> urlToProcess;
    private static List<BasicProxy> proxies;
    
    public static void initParameters() {
        try {
            try {
                //load instance
                GeneralUtils.loadInstance();
                //load Filters
                GeneralUtils.loadFilters();
                
                if( MetaData.SITE.equals("NONE")) {
                    System.out.println("Site has not been provided, Exiting.");                    
                }
                
            } catch (IOException ex) {
                System.out.println("Error Loading Config Files.");
                Logger.getLogger(Control.class.getName()).log(Level.SEVERE, null, ex);

            }           
                        
            try {
                DBUtils.createH2Connection();
                DBUtils.createBasicLinkTable();

                //delete basic table records
                if (MetaData.DB_DELETE_BASIC_LINK_TABLE) {
                    DBUtils.deleteDataFromTable("basic_link");
                }

            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            //temporal files
             GeneralUtils.createDataTempDir(MetaData.DELETE_TEMP_FILES);        
            
             //Proxies
             if (MetaData.USE_PROXY) {
                 //get proxy list
                 try {
                     proxies = GeneralUtils.getProxies();
                     Reports.totalProxies = proxies.size();
                 } catch (Exception ex) {                 
                 }
                 
                 Reports.useProxies = true;                 
             }
             
             if (MetaData.CHECK_PROXY) {
                 //check proxy availability
                 for (int i = 0; i < proxies.size(); i++) {
                     if (!GeneralUtils.testConnection(proxies.get(i))) {
                        proxies.remove(i);
                     }                     
                 }
                 
                 Reports.validProxies = proxies.size();
             }          
             
             Reports.totalCategoriesAvailable = GeneralUtils.getCategories().size();
             Reports.totalSubCategoriesAvailable = GeneralUtils.getSubcategories().size();
             Reports.totalDMARegionsAvailable = GeneralUtils.getDmaRegions().size();
             
        } catch (Exception ex) {
            Logger.getLogger(Control.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public static void doStart() throws IOException, URISyntaxException, InterruptedException{
        Reports.start_time = System.currentTimeMillis();
        
        if (!MetaData.DO_PARSE_ONLY) {
                //download fresh files and parse them.
                //get Url to Process
            System.out.println("Start Scraping");
            List<URI> cat  = GeneralUtils.setUpUrlsFromDMAandCategories();
            Reports.totalInitialLinksGenerated += cat.size();
            //run scraping
            doScraping(cat);
            //parse
            List<URI> next_pages = doParseRecords();
            
            while(!next_pages.isEmpty()) {
                System.out.println("Next Round...");
                doScraping(next_pages);
                next_pages = doParseRecords();                
            }
            
            System.out.println("Scraping Done.");
        } else {
           //Parsing only
            doParseRecords();
        }
        
        Reports.end_time = System.currentTimeMillis();
        
        
    }
//----------------------------------------------------------------------------------------------------------    
    public static void doScraping(List<URI> cat) throws IOException, InterruptedException 
    {
        SimpleRunner sr = new SimpleRunner();
        sr.proxies = proxies;
        sr.Run(cat);                   
    }
  //---------------------------------------------------------------------------------------------
    public static List<URI> doParseRecords() {
        //parsing Pool        
        
        //Next Round List
        List<URI> next_pages = new ArrayList<URI>();
        
        //Get Files to be Parsed
        File google[] = GeneralUtils.getFileArrayFromDir(MetaData.GOOGLE_DIR);
        File bing [] = GeneralUtils.getFileArrayFromDir(MetaData.BING_DIR);
        File yahoo [] = GeneralUtils.getFileArrayFromDir(MetaData.YAHOO_DIR);
        
        System.out.println("Google Files to be Parsed: " + google.length);
        System.out.println("Yahoo Files to be Parsed: " + yahoo.length);
        System.out.println("Bing Files to be Parsed: " + bing.length);
        
        //Thread Pool for Parsing
        ThreadParser poolParser[] = new ThreadParser[3];
        
        poolParser[0] = new ThreadParser(google, 0, next_pages);
        poolParser[1] = new ThreadParser(bing, 2, next_pages);
        poolParser[2] = new ThreadParser(yahoo, 1, next_pages);
        
        for (int i = 0; i < poolParser.length; i++) {            
                poolParser[i].start();                
            
        } // end for
       
        for (int i = 0; i < poolParser.length; i++) {            
            try {                
                poolParser[i].join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Control.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        } // end for
        
        //Number of next pages
        System.out.println(next_pages.size());
        
       //export records to txt
        GeneralUtils.exportBasicLinksToText("Data.txt");
        
        return next_pages;
    }
 
}
