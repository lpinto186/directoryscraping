/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.scrapingdirectories.base;

import java.io.File;
import java.net.URI;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class ThreadParser  extends Thread{
    protected File [] files;
    protected int engine;
    protected List<URI> global;

    public ThreadParser(File[] files, int engine, List<URI> global) {
        this.files = files;
        this.engine = engine;
        this.global = global;
    } 
    
    @Override
    public void run() {
        SimpleParser sp = new SimpleParser();
        SESimpleData se = null;
        for (int i = 0; i < files.length; i++) {
            try {
                se = new SESimpleData();                
                //split the file into the proper parser
                if (engine == 0) {
                    try {
                        se = sp.doParseGoogle(files[i]);
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                } else if (engine == 1) {
                    try {
                        se = sp.doParseYahoo(files[i]);
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                } else if (engine == 2) {
                    try {
                         se = sp.doParseBing(files[i]);
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                } 
                //check if there is next page
                if (se.next_page != null) {
                  global.add(se.next_page);
                }
            } catch (Exception ex) {
                Logger.getLogger(ThreadParser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
        
    }
    
}
