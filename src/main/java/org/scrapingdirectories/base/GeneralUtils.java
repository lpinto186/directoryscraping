/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.scrapingdirectories.base;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.scrapingdirectories.base.utils.DBUtils;

/**
 *
 * @author DELL
 */
public class GeneralUtils {
    
    
    public static List<String> getDmaRegions() throws IOException
    {
        return FileUtils.readLines(new File(MetaData.DMA_REGIONS_FILE));      
    }
    
    public static List<String> getDmaRegions(int min, int max) throws IOException
    {
        List<String> list = getDmaRegions();        
        return list.subList(min, max);      
    }
    
    public static List<String> getSubcategories() throws IOException
    {        
        return FileUtils.readLines(new File(MetaData.SUB_CATEGORIES_FILE));            
    }
    
    public static List<String> getSubcategories(int min, int max) throws IOException
    {        
        List<String> sub_categories = FileUtils.readLines(new File(MetaData.SUB_CATEGORIES_FILE));
        return sub_categories.subList(min, max);
    }
    
    public static List<String> getCategories() throws IOException
    {        
        return FileUtils.readLines(new File(MetaData.CATEGORIES_FILE));
    }
    
    public static List<String> getCategories(int min, int max) throws IOException
    {        
        List<String> categories = FileUtils.readLines(new File(MetaData.CATEGORIES_FILE));
        return categories.subList(min, max);
    }
    
    public static URI getGoogleQuery(String category, String location, String site) throws URISyntaxException
    {       
        URI uri = new URIBuilder()
        .setScheme("https")
        .setHost("www.google.com")
        .setPath("/search")
        .setParameter("q", category+" in " + location + " site:" + site)
        .setParameter("btnG", "Google Search")
        .setParameter("aq", "f")
        .setParameter("oq", "")
        .build();
        
        System.out.println("Google URI: " + uri);
        return uri;
    }
    
    public static URI getYahooQuery(String category, String location, String site) throws URISyntaxException
    {
        URI uri = new URIBuilder()
        .setScheme("https")
        .setHost("search.yahoo.com")
        .setPath("/search")
        .setParameter("p", category+" in " + location + " site:" + site)        
        .build();
        
        System.out.println("Yahoo URI: " + uri);
        return uri;
    }
    
    
    public static URI getBingQuery(String category, String location, String site) throws URISyntaxException
    {
        URI uri = new URIBuilder()
        .setScheme("http")
        .setHost("www.bing.com")
        .setPath("/search")
        .setParameter("q", category+" in " + location + " site:" + site)
        .setParameter("qs", "ds")
        .setParameter("go", "Serch")        
        .setParameter("form", "QBLH")
        .build();
        
        System.out.println("Bing URI: " + uri);
        return uri;
    }
    
    public static void createDataTempDir(boolean clean) {
        File f = new File(MetaData.BING_DIR);
        f.mkdirs();

        if (clean) {
            deleteFilesFromDir(f);
        }

        f = new File(MetaData.GOOGLE_DIR);
        f.mkdirs();

        if (clean) {
            deleteFilesFromDir(f);
        }

        f = new File(MetaData.YAHOO_DIR);
        f.mkdirs();

        if (clean) {
            deleteFilesFromDir(f);
        }

        f = new File(MetaData.OTHER_DIR);
        f.mkdirs();

        if (clean) {
            deleteFilesFromDir(f);
        }
        
        f = new File(MetaData.TXT_TEMP_OUTPUT);
        if (clean) {
           f.delete();
        }
        
        System.out.println("Data/temp directories created.");
    }
    
    private static void deleteFilesFromDir(File f) {
        File files[] = f.listFiles();
        for (int i = 0; i < files.length; i++) {
            files[i].delete();
        }
    }
    
    public static File[] getFileArrayFromDir(String dir)
    {
       File[] list = null; 
       
        if (MetaData.DO_PARSE_ONLY) {
            list = new File(dir).listFiles();
        } else {
            list = new File(dir).listFiles(new FilenameFilter() {

                @Override
                public boolean accept(File dir, String name) {
                    return !name.startsWith("parsed");
                }
            });
        }
        //Avoid parsedFiles
      return list;
    }
    
    public static void exportBasicLinksToText(String exportFile){
        try {
            List<String> links = DBUtils.getAllLinks();
            System.out.println("Links to be exported: "+links.size());
            Reports.totalExportedLinks = links.size();
            FileUtils.writeLines(new File(exportFile), links, false);
        } catch (Exception ex) {
            Logger.getLogger(GeneralUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
//----------------------------------------------------------------------------------    
    public static boolean applyContainsFilters(String link) {
       MetaData.linkFilters.add("/b/");
       boolean contains = false;
       
        for (String filter : MetaData.linkFilters) {
            if (link.indexOf(filter) > -1){
              contains = true;
              break;
            }
        }
        
        return contains;
    }
//------------------------------------------------------------------------------------    
    public static void loadFilters() throws IOException {
        List<String> filters = FileUtils.readLines(new File(MetaData.FILTERS_CONFIG_FILE));
        Iterator<String> it = filters.iterator();
        
        while(it.hasNext()) {
            String next = it.next();
            if(!next.contains("#")) {
               MetaData.linkFilters.add(next.split("=")[1].trim());
            }            
        }
    }
 //------------------------------------------------------------------------------------   
    public static void loadInstance() throws IOException {
        List<String> filters = FileUtils.readLines(new File(MetaData.INSTANCE_CONFIG_FILE));
        Iterator<String> it = filters.iterator();
        
        while(it.hasNext()) {
            String next = it.next();
            if(!next.contains("#") && next.length() > 5 ) {
               String key = next.split("=")[0].trim();
               String value = next.split("=")[1].trim();
               
               if(key.equalsIgnoreCase("cat_index_from")) {
                   try {
                      MetaData.CAT_FROM = Integer.parseInt(value);
                   } catch (Exception ex) {
                       System.out.println("Error Setting cat_index_from Parameter, default value 0");
                       MetaData.CAT_FROM = 0;
                   }
               } else if(key.equalsIgnoreCase("cat_index_to")) {
                   try {
                      MetaData.CAT_TO = Integer.parseInt(value);
                   } catch (Exception ex) {
                       System.out.println("Error Setting cat_index_to Parameter, default value ALL");
                       MetaData.CAT_TO = -1;
                   }
               } else if(key.equalsIgnoreCase("dma_region_from")) {
                   try {
                      MetaData.DMA_FROM = Integer.parseInt(value);
                   } catch (Exception ex) {
                       System.out.println("Error Setting dma_region_from Parameter, default value 0");
                       MetaData.DMA_FROM = 0;
                   }
               } else if(key.equalsIgnoreCase("dma_region_to")) {
                   try {
                      MetaData.DMA_TO = Integer.parseInt(value);
                   } catch (Exception ex) {
                       System.out.println("Error Setting dma_region_to Parameter, default value ALL");
                       MetaData.DMA_TO = -1;
                   }
               } else if(key.equalsIgnoreCase("use_category")) {
                   MetaData.USE_CATEGORY = Boolean.parseBoolean(value);
               } else if(key.equalsIgnoreCase("site")) {
                   MetaData.SITE = value;
               } else if(key.equalsIgnoreCase("db_delete_basic_link")) {
                   MetaData.DB_DELETE_BASIC_LINK_TABLE = Boolean.parseBoolean(value);
               } else if(key.equalsIgnoreCase("delete_txt_file_at_start")) {
                   MetaData.DELETE_TXT_OUTPUT_FILE_AT_START = Boolean.parseBoolean(value);
               } else if(key.equalsIgnoreCase("txt_file_path")) {
                   MetaData.TXT_TEMP_OUTPUT = value;
               } else if(key.equalsIgnoreCase("delete_temporal_data_files")) {
                   MetaData.DELETE_TEMP_FILES = Boolean.parseBoolean(value);
               } else if(key.equalsIgnoreCase("google_dir")) {
                   MetaData.GOOGLE_DIR = value;
               } else if(key.equalsIgnoreCase("yahoo_dir")) {
                   MetaData.YAHOO_DIR = value;
               } else if(key.equalsIgnoreCase("bing_dir")) {
                   MetaData.BING_DIR = value;
               } else if(key.equalsIgnoreCase("other_dir")) {
                   MetaData.OTHER_DIR = value;
               } else if(key.equalsIgnoreCase("parse_only")) {
                   MetaData.DO_PARSE_ONLY = Boolean.parseBoolean(value);
               } else if(key.equalsIgnoreCase("proxy_file")) {
                   MetaData.PROXY_FILE = value;
               } else if(key.equalsIgnoreCase("use_proxies")) {
                   MetaData.USE_PROXY = Boolean.parseBoolean(value);
               } else if(key.equalsIgnoreCase("check_proxies")) {
                   MetaData.CHECK_PROXY = Boolean.parseBoolean(value);
               } else if(key.equalsIgnoreCase("MAX_POOL_THREAD")) {
                   MetaData.MAX_POOL_THREAD = Integer.parseInt(value);
               }
               
               
            }            
        }
    }
 //-------------------------------------------------------------------------------------------------------------------   
     public static List<URI> setUpUrlsFromDMAandCategories() throws IOException, URISyntaxException
     {
         List<URI> processUrl = new ArrayList<URI> ();
         //get dma list if max_dma is -1 then we use all records available
          List<String> dma= null;
          
          if (MetaData.DMA_TO == -1) {
             dma = GeneralUtils.getDmaRegions();
          } else {
            dma = GeneralUtils.getDmaRegions(MetaData.DMA_FROM, MetaData.DMA_TO);
          }
          
          //get categories list, -1 for full categories
          List<String> cat = null;

         if (MetaData.CAT_TO == -1) {
             if (MetaData.USE_CATEGORY) {
                 cat = GeneralUtils.getCategories();
             } else {
                 cat = GeneralUtils.getSubcategories();
             }
         } else {

             if (MetaData.USE_CATEGORY) {
                 cat = GeneralUtils.getCategories(MetaData.CAT_FROM, MetaData.CAT_TO);
             } else {
                 cat = GeneralUtils.getSubcategories(MetaData.CAT_FROM, MetaData.CAT_TO);
             }
         }           
        
          //build the urls, one google, another yahoo and bing.
          int control = 0; //0 = google, 1 = yahoo, 2 = bing
          for (int i = 0; i < dma.size(); i++) {
              for (int j = 0; j < cat.size(); j++) {
                  if (control == 0) {                      
                      processUrl.add(GeneralUtils.getGoogleQuery(cat.get(j), dma.get(i), MetaData.SITE));
                  } else if (control == 1) {
                     processUrl.add(GeneralUtils.getYahooQuery(cat.get(j), dma.get(i), MetaData.SITE)); 
                  } else if (control == 2) {
                     processUrl.add(GeneralUtils.getBingQuery(cat.get(j), dma.get(i), MetaData.SITE));
                     control = -1;
                  }
                  
                  control++;
              }
         }
          
          System.out.println("DMA regions: "+dma.size() + " Category Locations: "+cat.size() + " Urls Built: " + processUrl.size());
          return processUrl;
     }   
    
     public static void printReport () {
         System.out.println("*******************************************************");
         System.out.println("Total Categories Available: " + Reports.totalCategoriesAvailable);
         System.out.println("Total SubCategories Available: " + Reports.totalSubCategoriesAvailable);
         System.out.println("Total DMI Regions Available: " + Reports.totalDMARegionsAvailable);
         System.out.println("Total Initial Links Set: " + Reports.totalInitialLinksGenerated);
         System.out.println("Total Scraped Links: " + Reports.totalSitesScraped);
         System.out.println("Total Sites Parsed: " + Reports.totalSitesParsed);
         System.out.println("Total Captcha Found: " + Reports.totalCaptchaResponse);
         System.out.println("Total Link Stored: " + Reports.totalLinksRecordsStored);
         System.out.println("Total Exported Links: " + Reports.totalExportedLinks);
         System.out.println("Use Proxies: " + Reports.useProxies);
         System.out.println("Total of Valid Proxies: " + Reports.validProxies);
         System.out.println("Total Proxies: " + Reports.totalProxies);
         System.out.println("Total time: " + (Reports.end_time - Reports.start_time) / 1000 + " secs.");
         System.out.println("*******************************************************");
     }
     
  //-------------------------------------------------------------------------------------------------------
     public static List<BasicProxy> getProxies() throws IOException 
     {
         List<BasicProxy> basic = new ArrayList<>();
         List<String> list = FileUtils.readLines(new File(MetaData.PROXY_FILE));
         
         for (String s : list) {
             try {
                 basic.add(new BasicProxy(s.split(":")[0].trim(), Integer.parseInt(s.split(":")[1].trim())));
             } catch (Exception ex) {
             }
         }
         
         return basic;
     }
   //--------------------------------------------------------------------------------  
    public static boolean testConnection(BasicProxy p) {
boolean connectionStatus = false;
       CloseableHttpClient httpclient = HttpClients.createDefault();
    try {
        HttpHost target = new HttpHost("google.com", 443, "https");
        HttpHost proxy = new HttpHost(p.server, p.port, "http");

        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        HttpGet request = new HttpGet("/");
        request.setConfig(config);

        System.out.println("Checking proxy request " + request.getRequestLine() + " to " + target + " via " + proxy);

        CloseableHttpResponse response = httpclient.execute(target, request);
        try {
            System.out.println("----------------------------------------");
            System.out.println(response.getStatusLine());
            
            if(response.getStatusLine().getStatusCode() == 200) {
              connectionStatus = true;
            }
            
            EntityUtils.consume(response.getEntity());
            
        } finally {
            response.close();
             httpclient.close();
        }
    }   catch (IOException ex) {
            Logger.getLogger(GeneralUtils.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
       
    }

        return connectionStatus;
    }
}
