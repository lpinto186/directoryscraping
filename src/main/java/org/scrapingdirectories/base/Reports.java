/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.scrapingdirectories.base;

/**
 *
 * @author DELL
 */
public class Reports {
    public static long start_time;
    public static int totalCategoriesAvailable = 0;
    public static int totalSubCategoriesAvailable = 0;
    public static int totalDMARegionsAvailable = 0;
    public static int totalInitialLinksGenerated = 0;
    
    public static int totalSitesScraped = 0;
    public static int totalSitesParsed = 0;
    public static int totalCaptchaResponse = 0;
    
    public static int totalExportedLinks = 0; 
    public static int totalLinksRecordsStored = 0;
    public static boolean useProxies = false;
    public static int totalProxies = 0;
    public static int validProxies = 0;
    
    public static long end_time;
}
