/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.scrapingdirectories.base;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author DELL
 */
public abstract class BasicReader {
    
    public String readSimple(String url, File file) throws IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String raw_data = new String();
        try {
            HttpGet request = new HttpGet(url);
            CloseableHttpResponse response = httpclient.execute(request);
            try {                
                System.out.println(response.getStatusLine());
                raw_data = EntityUtils.toString(response.getEntity());
                
                if(file != null) {
                  FileUtils.writeStringToFile(file, raw_data);
                }
                
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }

        return raw_data;
    }
    
    
    
}
