/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.scrapingdirectories.base;

import java.io.File;
import java.util.List;
import java.util.Random;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author DELL
 */
public class GetThread extends Thread{
    
    private final CloseableHttpClient httpClient;
        private final HttpContext context;
        private final HttpGet httpget;
        private final int id;
        protected List<BasicProxy> proxies;
        
        public GetThread(CloseableHttpClient httpClient, HttpGet httpget, int id) {
            this.httpClient = httpClient;
            this.context = new BasicHttpContext();
            this.httpget = httpget;
            this.id = id;
        }

    public GetThread(CloseableHttpClient httpClient, HttpGet httpget, int id, List<BasicProxy> proxies) {
        this.httpClient = httpClient;       
        this.httpget = httpget;
        this.context = new BasicHttpContext();
        this.id = id;
        this.proxies = proxies;
    }

        
        /**
         * Executes the GetMethod and prints some status information.
         */
        @Override
        public void run() {
            try {
                System.out.println(id + " - about to get something from " + httpget.getURI());
                CloseableHttpResponse response = null;
                BasicProxy p = null;
                if (MetaData.USE_PROXY && proxies != null && !proxies.isEmpty()) {
                    p = randomProxy();
                    HttpHost proxy = new HttpHost(p.server, p.port, "http");

                    RequestConfig config = RequestConfig.custom()
                            .setProxy(proxy)
                            .build();
                    
                    httpget.setConfig(config);                    
                    response = httpClient.execute(httpget, context);
                } else {
                    response = httpClient.execute(httpget, context);
                }
                
                
                try {
                    System.out.println(id + " "+ httpget.getURI()+" - get executed");
                    // get the response body as an array of bytes
                    if (response.getStatusLine().getStatusCode() != 200 ) {
                        return;
                    }
                    
                    Reports.totalSitesScraped++;
                    
                    HttpEntity entity = response.getEntity();
                    if (entity != null) {                   
                        String raw_data = EntityUtils.toString(response.getEntity());
                        
                        if (httpget.getURI().getHost().contains("bing")) {
                           FileUtils.writeStringToFile(new File(MetaData.BING_DIR+id+".html"), raw_data);
                        } else if (httpget.getURI().getHost().contains("yahoo")) {
                            FileUtils.writeStringToFile(new File(MetaData.YAHOO_DIR+id+".html"), raw_data);
                        } else if (httpget.getURI().getHost().contains("google")) {
                            FileUtils.writeStringToFile(new File(MetaData.GOOGLE_DIR+id+".html"), raw_data);
                        } else  {
                            FileUtils.writeStringToFile(new File(MetaData.OTHER_DIR+id+".html"), raw_data);
                        }
                        
                        
                        System.out.println(id + " - " + response.getStatusLine());
                    }
                } finally {
                    response.close();
                }
            } catch (Exception e) {
                System.out.println(id + " - error: " + e);
            }
        }
     //---------------------------------------------------------
     public  BasicProxy randomProxy() {
        Random r = new Random();
        return proxies.get(r.nextInt(proxies.size()));
     }
}
